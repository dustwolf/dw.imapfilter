# Ansible IMAPFilter

Ansible role for installing and configuring the imapfilter mailbox rules program.

Currently made to work with CentOS 8 stream Linux.

## Project status
This is a work in progress. I am using this to manage my own private servers and I will contribute as time allows.

You may use this code if you find it useful.

## Dependencies

Obviously this doesn't do much without an IMAP server.

Requires dw.dovecot: https://gitlab.com/dustwolf/dw.dovecot
